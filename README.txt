=== The City  ===
Contributors: hailstorm
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This modules contains submodules for interacting with The City software
 (http://developer.onthecity.org/)

Currently it includes the following submodules:

- The City Login: A module that allows users to login directly to The City from
  a Drupal website.Currently this module contains a submodule which provides a
  block with a form loaded from Javascript.


When the City's OAUTH API is released this module will contain other submodules
which will act as a wrapper to the City's API.


== Installation & Usage ==

Install and enable thecity_login module as usual,
see http://drupal.org/node/895232 for further information.

The City login
--------------
Configure the form settings (including the city account you want the form to
work on) at admin/config/services/thecity.

Essentially, This module provides a Block which can be placed into regions.
Alternatively, you can use the theme function theme('login_placeholder').


== Contact ==

Current maintainers:
* Andy Hails (hailstorm) - http://drupal.org/user/786862
