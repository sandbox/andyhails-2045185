<?php

/**
 * @file
 * Administration page callbacks for TheCity module.
 */

/**
 * Form constructor for TheCity settings form.
 */
function thecity_login_admin_form($form, &$form_state) {

  $form['thecity_login_subdomain_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Subdomain'),
    '#description' => t('The part of the URL before the "OnTheCity.org". For example in the URL http://<em>cck</em>.onthecity.org the subdomain would be <em>cck</em>.'),
    '#default_value' => variable_get('thecity_login_subdomain_key'),
    '#required' => TRUE,
  );

  $options = array('plain', 'inline');
  $form['thecity_login_login_display_choice'] = array(
    '#type' => 'select',
    '#title' => t('Display'),
    '#description' => t('Form display style.'),
    '#options' => drupal_map_assoc($options, 'drupal_ucfirst'),
    '#default_value' => variable_get('thecity_login_login_display_choice', 'plain'),
  );

  $form['thecity_login_form_loading_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show loading message'),
    '#description' => t('"Loading login form..."'),
    '#default_value' => variable_get('thecity_login_form_loading_message'),
  );

  $form['thecity_login_show_remember_me'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Remember me checkbox'),
    '#default_value' => variable_get('thecity_login_show_remember_me'),
  );

  $form['thecity_login_use_placeholder_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use placeholder text'),
    '#description' => t('Use HTML5 placeholder attribute instead of label.'),
    '#default_value' => variable_get('thecity_login_use_placeholder_text'),
  );

  return system_settings_form($form);
}
