(function ($) {

/**
 * Attaches the behaviors for the theCity module.
 */

Drupal.behaviors.theCity = {
  attach: function (context, settings) {
    $(function() {
      TheCityLogin.start({
        'subdomain' : Drupal.settings.theCity.subdomain,
        'display_form_loading_message' : Drupal.settings.theCity.formLoadingMessage,
        'show_remember_me' : Drupal.settings.theCity.showRememberMe,
        'use_placeholder_text' : Drupal.settings.theCity.usePlaceholderText
      });
    });
  }
};

})(jQuery);


