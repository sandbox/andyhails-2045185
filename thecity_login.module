<?php
/**
 * @file
 * Allows users to provide to log into their City website from a Drupal installation.
 */

/**
 * Implements hook_menu().
 */
function thecity_login_menu() {
  $items['admin/config/services/thecity'] = array(
    'title' => 'The City settings',
    'description' => 'Settings for The City integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('thecity_login_admin_form'),
    'access arguments' => array('access site configuration'),
    'file' => 'thecity_login.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function thecity_login_block_info() {
  $blocks['thecity_login'] = array(
    'info' => t('The City Login'),
    'cache' => DRUPAL_NO_CACHE
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function thecity_login_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'thecity_login':
      $block['subject'] = t('The City login');
      $block['content'] = theme('login_placeholder');
      break;

  }
  return $block;
}

/**
 * Implements hook_theme().
 */
function thecity_login_theme($existing, $type, $theme, $path) {
  return array(
    'login_placeholder' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Preprocess function for login_placeholder
 */
function thecity_login_preprocess_login_placeholder() {
  $settings = array(
    'subdomain' => variable_get('thecity_login_subdomain_key'),
    'formLoadingMessage' => variable_get('thecity_login_form_loading_message', FALSE),
    'showRememberMe' => variable_get('thecity_login_show_remember_me', FALSE),
    'usePlaceholderText' => variable_get('thecity_login_use_placeholder_text', FALSE)
  );

  // Add resources
  drupal_add_js(array('theCity' => $settings), 'setting');
  drupal_add_js(drupal_get_path('module', 'thecity_login') . '/js/thecity_login_library.js');
  drupal_add_js(drupal_get_path('module', 'thecity_login') . '/thecity_login.js');
  drupal_add_css(drupal_get_path('module', 'thecity_login') . '/css/thecity_login.css');
}

/**
 * Markup for login_placeholder
 */
function theme_login_placeholder() {
  // Add theme class
  $classes = array();
  switch (variable_get('thecity_login_login_display_choice')) {
    case 'inline':
      $classes[] = 'thecity_login_inline_plain';
      break;
    default:
      $classes[] = 'thecity_login_normal_plain';
      break;
  }

  return '<div id="thecity_login" class="' . implode(" ", $classes) . '"></div>';
}
